﻿using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WorkerRole1
{
    public class SimpleEventProcessor : IEventProcessor
    {
        IDictionary<string, int> map;
        PartitionContext partitionContext;
        Stopwatch checkpointStopWatch;

        public SimpleEventProcessor()
        {
            this.map = new Dictionary<string, int>();
        }

        public Task OpenAsync(PartitionContext context)
        {
            Trace.TraceInformation(string.Format("SimpleEventProcessor initialize.  Partition: '{0}', Offset: '{1}'", context.Lease.PartitionId, context.Lease.Offset));
            this.partitionContext = context;
            this.checkpointStopWatch = new Stopwatch();
            this.checkpointStopWatch.Start();
            return Task.FromResult<object>(null);
        }

        public async Task ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> events)
        {
            try
            {
                foreach (EventData eventData in events)
                {
                    int data;
                    var ReciveData = this.DeserializeEventDataCTD(eventData);
                    string key = eventData.PartitionKey;

                    // Name of device generating the event acts as hash key to retrieve average computed for it so far
                    if (!this.map.TryGetValue(key, out data))
                    {
                        // If this is the first time we got data for this device then generate new state
                        this.map.Add(key, -1);
                    }

                    // Update data
                    data = Convert.ToInt32(ReciveData.temp);
                    this.map[key] = data;

                    Trace.TraceInformation(string.Format("Data received. Partition: '{0}', Device: '{1}', TEMP: '{2}', HUMI: '{6}', Offset: '{3}', SequenceNumber: '{4}', UTCTime: '{5}'",
                       this.partitionContext.Lease.PartitionId, key, data, eventData.Offset, eventData.SequenceNumber, eventData.EnqueuedTimeUtc, ReciveData.hmdt));

                    // Write temperature data to local space.
                    using (StreamWriter sw = new StreamWriter(@"temp.txt"))
                    {
                        // Add some text to the file.
                        sw.Write(data);
                    }

                }

                // Call checkpoint every 5 minutes, so that worker can resume processing from the 5 minutes back if it restarts.
                if (this.checkpointStopWatch.Elapsed > TimeSpan.FromMinutes(5))
                {
                    await context.CheckpointAsync();
                    this.checkpointStopWatch.Restart();
                }
            }
            catch (Exception exp)
            {
                Trace.TraceInformation("Error in processing: " + exp.Message);
            }
        }

        public async Task CloseAsync(PartitionContext context, CloseReason reason)
        {
            Trace.TraceInformation(string.Format("Processor Shuting Down.  Partition '{0}', Reason: '{1}'.", this.partitionContext.Lease.PartitionId, reason.ToString()));
            if (reason == CloseReason.Shutdown)
            {
                await context.CheckpointAsync();
            }
        }

        ctdEvent DeserializeEventDataCTD(EventData eventData)
        {
            // CTD format
            // String.Format("{{ \"temp\" : {0}, \"hmdt\" : {1}, \"lght\" : {2}, \"DeviceGUID\" : \"{3}\", \"Subject\" : \"{4}\", \"dspl\" : \"{5}\"}}", 

            return JsonConvert.DeserializeObject<ctdEvent>(Encoding.UTF8.GetString(eventData.GetBytes()));
        }

        public class ctdEvent
        {
            [DataMember]
            public double temp { get; set; }

            [DataMember]
            public double lght { get; set; }

            [DataMember]
            public string DeviceGUID { get; set; }

            [DataMember]
            public string Subject { get; set; }

            [DataMember]
            public string dspl { get; set; }

            [DataMember]
            public string hmdt { get; set; }


        }
    }
}
