﻿using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerRole1
{
    class Receiver
    {
        #region Fields
        string eventHubName;
        EventHubConsumerGroup defaultConsumerGroup;

        string eventHubConnectionString;
        EventProcessorHost eventProcessorHost;
        #endregion

        public Receiver(string eventHubName, string eventHubConnectionString)
        {
            this.eventHubConnectionString = eventHubConnectionString;
            this.eventHubName = eventHubName;
        }

        public async Task MessageProcessingWithPartitionDistribution()
        {

            EventHubClient eventHubClient = EventHubClient.CreateFromConnectionString(eventHubConnectionString, this.eventHubName);

            // Get the default Consumer Group
            defaultConsumerGroup = eventHubClient.GetDefaultConsumerGroup();
            string blobConnectionString = CloudConfigurationManager.GetSetting("StorageConnectionString"); // Required for checkpoint/state
            eventProcessorHost = new EventProcessorHost("singleworker",
                                                        eventHubClient.Path,
                                                       "App01",
                                                        this.eventHubConnectionString,
                                                        blobConnectionString,
                                                        "ehdevices");

            // Read EvnetData from EventHubs, adjust offset to latest position, make sure won't get old data.
            EventProcessorOptions eventProcessorOptions = new EventProcessorOptions();
            eventProcessorOptions.InitialOffsetProvider = (partitionId) => DateTime.UtcNow;

            Trace.TraceInformation(">>>Registering Event Processor, Please wait.<<<");

            await eventProcessorHost.RegisterEventProcessorAsync<SimpleEventProcessor>(eventProcessorOptions);

        }


        public void UnregisterEventProcessor()
        {
            eventProcessorHost.UnregisterEventProcessorAsync().Wait();
        }
    }
}
