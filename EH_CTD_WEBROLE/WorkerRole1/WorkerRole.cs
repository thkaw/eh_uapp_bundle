using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;

namespace WorkerRole1
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);


        private CloudBlobContainer container;


        static string eventHubName;
        static int numberOfMessages;
        static int numberOfPartitions;

        public override void Run()
        {
            Trace.TraceInformation("WorkerRole1 is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.



            Trace.TraceInformation("WorkerRole1 has been started");

            CloudStorageAccount storageAccount =
                CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));


            Trace.TraceInformation("check point1:" + storageAccount.BlobEndpoint.ToString());

            // initialize blob storage
            CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();

            // Create a container name "eventlogs"
            container = blobStorage.GetContainerReference("eventlogs");

            Trace.TraceInformation("Creating container...");

            bool storageInitialized = false;
            while (!storageInitialized)
            {
                try
                {

                    // create the blob container and allow public access
                    container.CreateIfNotExists();

                    container.SetPermissions(
                        new BlobContainerPermissions
                        {
                            PublicAccess =
                                BlobContainerPublicAccessType.Blob
                        });

                    storageInitialized = true;
                }
                catch (StorageException e)
                {
                    if (e != null)
                    {
                        Trace.TraceError("Storage services initialization failure. "
                          + "Check your storage account configuration settings. If running locally, "
                          + "ensure that the Development Storage service is running. Message: '{0}'", e.Message);
                        System.Threading.Thread.Sleep(5000);
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            bool result = base.OnStart();

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("WorkerRole1 is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("WorkerRole1 has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {

            Trace.TraceInformation("CTD CONSOLE RECIVER IN WORKERROLE. 2015/03/24");

            ParseArgs(new string[] { "ehdevices", "10", "8" });

            string connectionString = GetServiceBusConnectionString();
            NamespaceManager namespaceManager = NamespaceManager.CreateFromConnectionString(connectionString);

            Receiver r = new Receiver(eventHubName, connectionString);
            r.MessageProcessingWithPartitionDistribution().Wait();




            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");

                CloudBlockBlob blockBlob = container.GetBlockBlobReference("tempblob.txt");

                // Period upload EventHub data to blob.
                using (var fileStream = System.IO.File.OpenRead(@"temp.txt"))
                {
                    blockBlob.UploadFromStream(fileStream);
                }

                await Task.Delay(5000);
            }
        }

        static void ParseArgs(string[] args)
        {
            if (args.Length < 2)
            {
                throw new ArgumentException("Incorrect number of arguments. Expected 3 args <eventhubname> <NumberOfMessagesToSend> <NumberOfPartitions>", args.ToString());
            }
            else
            {
                eventHubName = args[0];
                Trace.TraceInformation("ehnanme: " + eventHubName);

                numberOfMessages = Int32.Parse(args[1]);
                Trace.TraceInformation("NumberOfmessage: " + numberOfMessages);

                numberOfPartitions = Int32.Parse(args[2]);
                Trace.TraceInformation("numberOfPartitions: " + numberOfPartitions);
            }
        }

        private static string GetServiceBusConnectionString()
        {
            string connectionString = CloudConfigurationManager.GetSetting("Microsoft.ServiceBus.ConnectionString");
            if (string.IsNullOrEmpty(connectionString))
            {
                Trace.TraceInformation("Did not find Service Bus connections string in appsettings (app.config)");
                return string.Empty;
            }
            ServiceBusConnectionStringBuilder builder = new ServiceBusConnectionStringBuilder(connectionString);
            builder.TransportType = Microsoft.ServiceBus.Messaging.TransportType.Amqp;
            return builder.ToString();
        }



    }
}
