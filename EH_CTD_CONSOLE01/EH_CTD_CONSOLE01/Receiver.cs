﻿using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EH_CTD_CONSOLE01
{
    class Receiver
    {
        #region Fields
        string eventHubName;
        EventHubConsumerGroup defaultConsumerGroup;

        string eventHubConnectionString;
        EventProcessorHost eventProcessorHost;
        #endregion

        public Receiver(string eventHubName, string eventHubConnectionString)
        {
            this.eventHubConnectionString = eventHubConnectionString;
            this.eventHubName = eventHubName;
        }

        public async Task MessageProcessingWithPartitionDistribution()
        {


            EventHubClient eventHubClient = EventHubClient.CreateFromConnectionString(eventHubConnectionString, this.eventHubName);

            // Get the default Consumer Group
            defaultConsumerGroup = eventHubClient.GetDefaultConsumerGroup();
            string blobConnectionString = ConfigurationManager.AppSettings["AzureStorageConnectionString"]; // Required for checkpoint/state
            eventProcessorHost = new EventProcessorHost("singleworker",
                                                        eventHubClient.Path,
                // defaultConsumerGroup.GroupName,      //記得不能用default的ConsumberGroup，會無法套用下面的InitalOffsetProvicder
                                                       "App01",
                                                        this.eventHubConnectionString,
                                                        blobConnectionString,
                                                        "ehdevices");

            //讀取eventdata的時候，保持最新的時間位置，這樣才不會讀取到一堆舊資料  
            EventProcessorOptions eventProcessorOptions = new EventProcessorOptions();
            eventProcessorOptions.InitialOffsetProvider = (partitionId) => DateTime.UtcNow;

            //var eventProcessorOptions = new EventProcessorOptions
            //{
            //    InitialOffsetProvider = partitionId =>
            //    {
            //        var lease = EventProcessorCheckpointHelper.GetLease("dxrdaa01suki-ns",
            //                                                         "ehdevices",
            //                                                         partitionId);
            //        return lease != null ? lease.Offset : "-1";
            //    },                                             
            //};

            Console.WriteLine(">>>Registering Event Processor, Please wait.<<<");

            await eventProcessorHost.RegisterEventProcessorAsync<SimpleEventProcessor>(eventProcessorOptions);





        }


        public void UnregisterEventProcessor()
        {
            eventProcessorHost.UnregisterEventProcessorAsync().Wait();
        }
    }
}
