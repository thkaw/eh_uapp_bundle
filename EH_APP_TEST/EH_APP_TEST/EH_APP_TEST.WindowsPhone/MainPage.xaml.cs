﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinRTXamlToolkit.Controls.DataVisualization.Charting;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace EH_APP_TEST
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private DispatcherTimer timer;


        public MainPage()
        {
            this.InitializeComponent();

            timer = new DispatcherTimer();
            timer.Tick += timer_Tick;

            //Default refresh time is 5 seconds.
            timer.Interval = TimeSpan.FromSeconds(5);
            UpdateCharts(TempList);
        }

        // Using ObservableCollection to live update line chart data.
        ObservableCollection<NameValueItem> TempList = new ObservableCollection<NameValueItem>();

        async void timer_Tick(object sender, object e)
        {

            // Using random addition value to avoid webdata cache.                                          
            Random rr = new Random();
            int rdn = rr.Next(1, 1000000);

            // TODO: Change to your site name.
            Uri uri = new Uri("http://dxrdaactdwcf.azurewebsites.net/Service1.svc/GetEHTemp?" + rdn);

            HttpClient httpClient = new HttpClient();

            string result = await httpClient.GetStringAsync(uri);

            XDocument doc = XDocument.Parse(result);

            tb_temp.Text = doc.Root.Value;

            TempList.Add(new NameValueItem { Name = DateTime.Now.ToString("HH:mm:ss"), Value = Convert.ToInt32(tb_temp.Text) });

            // Max node in line chart is 10, you can adjust this value.
            if (TempList.Count > 10)
            {
                TempList.RemoveAt(0);
            }

            UpdateCharts(TempList);

        }

        private void btn_period_Click(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            timer.Interval = TimeSpan.FromSeconds(Convert.ToInt32(tb_period.Text));
            timer.Start();
        }

        private void btn_start_Click(object sender, RoutedEventArgs e)
        {
            timer.Start();
        }

        private void UpdateCharts(ObservableCollection<NameValueItem> items)
        {
            ((LineSeries)this.LineChart.Series[0]).ItemsSource = items;
        }

        public class NameValueItem
        {
            public string Name { get; set; }
            public int Value { get; set; }
        }


    }
}
