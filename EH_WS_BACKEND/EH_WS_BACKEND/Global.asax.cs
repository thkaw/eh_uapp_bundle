﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace WebApplication5
{
    public class Global : System.Web.HttpApplication
    {

        private static string GetServiceBusConnectionString()
        {
            string connectionString = ConfigurationManager.AppSettings["Microsoft.ServiceBus.ConnectionString"];
            if (string.IsNullOrEmpty(connectionString))
            {
                Console.WriteLine("Did not find Service Bus connections string in appsettings (app.config)");
                return string.Empty;
            }
            ServiceBusConnectionStringBuilder builder = new ServiceBusConnectionStringBuilder(connectionString);
            builder.TransportType = TransportType.Amqp;
            return builder.ToString();
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            ////Initialize
            //Console.WriteLine("MSTW DXRDAA CTD CONSOLE RECIVER. 2015/02/24");
                                        
            //string connectionString = GetServiceBusConnectionString();
            //NamespaceManager namespaceManager = NamespaceManager.CreateFromConnectionString(connectionString);

            //Receiver r = new Receiver("ehdevices", connectionString);
            //r.MessageProcessingWithPartitionDistribution().Wait();

            //Console.WriteLine("Press enter key to stop worker.");
            //Console.ReadLine();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}