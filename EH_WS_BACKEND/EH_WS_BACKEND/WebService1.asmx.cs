﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebApplication5
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://www.ntex.tw/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string readEVTemp()
        {
            try
            {
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                     ConfigurationManager.AppSettings["StorageConnectionString"]);

                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference("eventlogs");

                // Retrieve reference to a blob named "tempblob.txt".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference("tempblob.txt");

                //using (var fileStream = System.IO.File.OpenWrite(@"temp.txt"))
                //{
                //    blockBlob.DownloadToStream(fileStream);
                //} 


                string temp = blockBlob.DownloadText();

                return temp;


                #region Read local file
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                //using (StreamReader sr = new StreamReader(@"D:\temp.txt"))     //小寫TXT
                //using (StreamReader sr = new StreamReader(@"temp.txt"))     //小寫TXT
                //{
                //    String line;
                //    // Read and display lines from the file until the end of 
                //    // the file is reached.
                //    while ((line = sr.ReadLine()) != null)
                //    {
                //        return line;
                //    }


                //    return "Blank";
                //}
                #endregion

            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);

                return "Error";
            }
        }


    }


}
